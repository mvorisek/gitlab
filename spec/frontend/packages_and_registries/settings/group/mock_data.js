export const groupPackageSettingsMock = {
  data: {
    group: {
      packageSettings: {
        mavenDuplicatesAllowed: true,
        mavenDuplicateExceptionRegex: '',
        __typename: 'PackageSettings',
      },
      __typename: 'Group',
    },
  },
};
